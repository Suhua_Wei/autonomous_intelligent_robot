#! /usr/bin/env python
import rospy
from geometry_msgs.msg  import Twist
from turtlesim.msg import Pose
from std_srvs.srv import *
from turtlesim.srv import *
from math import pow,atan2,sqrt, pi
from turtlesim.srv import *
import random
import sys

class HunterRunner():
    def __init__(self):
        rospy.init_node('assign2', anonymous=True)
        self.tolerance = 1
        self.runnerx = 0
        self.runnery = 0
        self.runnertheta = 0
        self.turtle1x = 0
        self.turtle1y = 0
        self.turtle1theta = 0
        self.distance = 1000000
        self.angular_x = random.uniform(-1,1)
        self.angular_y = random.uniform(-1,1)
        self.angular_z = random.uniform(-1,1)
       
        self.pubh = rospy.Publisher('/turtle1/cmd_vel', Twist,queue_size=10)
        self.pubr = rospy.Publisher('/runner/cmd_vel', Twist, queue_size=10)
        rospy.Subscriber('/turtle1/pose', Pose, self.turtle1_callback)
        rospy.Subscriber('/runner/pose', Pose, self.runner_callback)
        
        self.spawn = rospy.ServiceProxy('/spawn', Spawn)
        self.kill = rospy.ServiceProxy('/kill', Kill)
        self.clear = rospy.ServiceProxy('/clear', Empty)
        # self.reset = rospy.ServiceProxy('/reset', Empty)
        self.motionh = Twist()
        self.motionr = Twist()
        self.start = rospy.Time.now()

    def turtle1_callback(self, data):
        self.turtle1x = data.x
        self.turtle1y = data.y
        self.turtle1theta = data.theta

    def runner_callback(self, data):
        self.runnerx = data.x
        self.runnery = data.y
        self.turtle1theta = data.theta

    def create_turtle(self,turtleName):
        print "create new turtle "+turtleName
        if turtleName == 'turtle1':
            random.seed()
            self.turtle1x = random.randint(0,11)
            self.turtle1y = random.randint(0,11)
            self.spawn(self.turtle1x, self.turtle1y, 0, 'turtle1')
        elif turtleName == 'runner':
            random.seed()
            self.runnerx = random.randint(0,11)
            self.runnery = random.randint(0,11)
            self.spawn(self.runnerx, self.runnery, 0, 'runner')
        else:
            Sys.exit("wrong tutle name, use turtle1 or runner")


    def computer_distance(self):
        return sqrt(pow((self.runnerx-self.turtle1x),2) + 
            pow((self.runnery-self.turtle1y),2))

    def publish_control_signal(self):
      
        # computer the angle between turtle1 and runner
        angle = atan2(self.runnery-self.turtle1y, self.runnerx-self.turtle1x)
        self.motionh.angular.x = 0
        self.motionh.angular.y = 0
        self.motionh.angular.z = angle - self.angular_z - self.turtle1theta
        self.motionh.linear.x = 1
        self.motionh.linear.y = 0
        self.motionh.linear.z = 0
        self.pubh.publish(self.motionh)

        # control signal for the runner
    def publish_random_signal(self):
       

        #change angular speed very 2 second
        if rospy.Time.now() - self.start < rospy.Duration(2):
            pass
        else:
            self.start = rospy.Time.now()
            random.seed()
            self.angular_x = random.uniform(-1,1)
            self.angular_y = random.uniform(-1,1)
            self.angular_z = random.uniform(-1,1)

        self.motionr.angular.x = self.angular_x
        self.motionr.angular.y = self.angular_y
        self.motionr.angular.z = self.angular_z
        self.motionr.linear.x = 1
        self.motionr.linear.y = 0
        self.motionr.linear.z = 0
        self.pubr.publish(self.motionr)


    def run(self):
        while not rospy.is_shutdown():
            self.distance = self.computer_distance()
            if(self.distance < self.tolerance):
                rospy.wait_for_service('/kill')
                self.kill('runner')
                self.distance = 10000
                self.clear()
                rospy.wait_for_service('/spawn')
                self.create_turtle('runner')
            self.publish_control_signal()
            self.publish_random_signal()
            


if __name__ == '__main__':
    try:
        y = HunterRunner()
        y.create_turtle('runner')
        y.run()
    except rospy.ROSInterruptException: pass


